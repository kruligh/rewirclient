package com.yeahbunny.rewirclient;

import com.google.android.gms.location.LocationRequest;

/**
 * Created by Krolis on 2016-12-17.
 */

public class LocationRequestFactory {

    public static final float MINIMAL_DISTANCE = 5;

    public static LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(MINIMAL_DISTANCE);
        return mLocationRequest;
    }
}
