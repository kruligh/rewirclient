package com.yeahbunny.rewirclient;

import android.graphics.Color;

import java.util.Random;

public class RGBColor {
    private int red;
    private int green;
    private int blue;

    public RGBColor() {
        red = new Random().nextInt(255);
        green = new Random().nextInt(255);
        blue = new Random().nextInt(255);
    }

    public int getRed() {
        return red;
    }

    public void setRed(int color) {
        red = color;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int color) {
        green = color;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int color) {
        blue = color;
    }

    public int getColor() {
        return Color.rgb(red,green,blue);
    }
}
