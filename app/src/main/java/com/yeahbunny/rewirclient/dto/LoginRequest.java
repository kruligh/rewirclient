package com.yeahbunny.rewirclient.dto;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krolis on 2016-12-18.
 */
public class LoginRequest {

    private String login;
    private String password;

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String toJson() throws JSONException{

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("login",this.login);
            jsonObject.put("password", this.password);
            return jsonObject.toString();



    }
}
