package com.yeahbunny.rewirclient.dto;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krolis on 2016-12-18.
 */
public class Pixel {

    private double latitude;
    private double longitude;
    private int color;

    public Pixel() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public static List<Pixel> listFromJSON(String string) throws JSONException{

        List<Pixel> result = new ArrayList<>();

        JSONArray jsonArray = new JSONArray(string);

        for(int i =0; i<jsonArray.length();i++){
            result.add(fromJSONObject(jsonArray.getJSONObject(i)));
        }

        return result;

    }

    private static Pixel fromJSONObject(JSONObject jsonObject) throws JSONException{
        Pixel result = new Pixel();
        result.setLatitude(jsonObject.getInt("latitude")/10000.0);
        result.setLongitude(jsonObject.getInt("longitude")/10000.0);
        result.setColor(Color.parseColor(jsonObject.getJSONObject("user").getString("color")));
        return result;
    }
}
