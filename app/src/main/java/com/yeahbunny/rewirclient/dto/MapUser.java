package com.yeahbunny.rewirclient.dto;

import android.graphics.Color;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krolis on 2016-12-18.
 */
public class MapUser {

    private Long id;
    private int color;

    public MapUser() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public static MapUser fromJSONObject(JSONObject user) throws JSONException{
        MapUser result = new MapUser();

        result.setId(user.getLong("id"));
        result.setColor(Color.parseColor(user.getString("color")));
        return result;
    }
}
