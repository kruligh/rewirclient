package com.yeahbunny.rewirclient.dto;

import android.graphics.Color;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krolis on 2016-12-18.
 */
public class MyUser {

    private String username;
    private int color;
    private String points;

    public void setUsername(String username) {
        this.username = username;
    }
    public void setColor(String color) {
        this.color = Color.parseColor(color);
    }
    public void setPoints(String points) {
        this.points = points;
    }

    public String getUsername() {
        return username;
    }
    public int getUserColor() {
        return color;
    }
    public String getPoints() {
        return points;
    }

    public static MyUser fromJSON(String string) throws JSONException{
        MyUser result = new MyUser();
        JSONObject jsonObject = new JSONObject(string);
        result.setUsername(jsonObject.getString("username"));
        result.setColor(jsonObject.getString("color"));
        result.setPoints(jsonObject.getString("points"));
        return result;
    }
}
