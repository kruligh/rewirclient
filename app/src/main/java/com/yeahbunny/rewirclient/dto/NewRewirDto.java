package com.yeahbunny.rewirclient.dto;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Krolis on 2016-12-18.
 */

public class NewRewirDto{

    private List<LatLng> latLngList;

    public NewRewirDto(List<LatLng> list) {
        this.latLngList = list;
    }

    public String toJson() {
        JSONArray jsonArray = new JSONArray();
        for(LatLng latLng: latLngList){
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("latitude",latLng.latitude);
                jsonObject.put("longitude", latLng.longitude);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray.toString();
    }
}
