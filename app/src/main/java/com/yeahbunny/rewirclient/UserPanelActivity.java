package com.yeahbunny.rewirclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.yeahbunny.rewirclient.dto.LoginRequest;
import com.yeahbunny.rewirclient.dto.MyUser;
import com.yeahbunny.rewirclient.dto.NewRewirResult;

import org.json.JSONException;

import java.io.IOException;

public class UserPanelActivity extends AppCompatActivity {

    TextView tvUserName;
    TextView tvPoints;
    TextView tvIncome;
    ImageView ivUserColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvPoints = (TextView) findViewById(R.id.tvPoints);
        tvIncome = (TextView) findViewById(R.id.tvIncome);
        ivUserColor = (ImageView) findViewById(R.id.ivUserColor);
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.activity_user_panel);
        mainLayout.requestFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetMyUserAsyncTask().execute();

    }

    public void onClickShowRank(View view) {
        Intent intent = new Intent(this,RankActivity.class);
        startActivityForResult(intent,0);
    }

    public void onClickGoToMap(View view) {
        Intent intent = new Intent(this,MapsActivity.class);
        startActivityForResult(intent,0);
    }

    private class GetMyUserAsyncTask extends AsyncTask<Void,Void, MyUser> {

        private static final String url = Config.SERVER_URL+"/core/getMyUser";

        @Override
        protected MyUser doInBackground(Void... params) {
            try {
                System.out.println(url);
                OkHttpClient client = new OkHttpClient();

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                        getString(R.string.auth_token), Context.MODE_PRIVATE);

                Request request = new Request.Builder()
                      //  .header(getResources().getString(R.string.auth_token), sharedPref.getString(getResources().getString(R.string.auth_token),""))
                        .url(url)
                        .addHeader(getResources().getString(R.string.auth_token), sharedPref.getString(getResources().getString(R.string.auth_token),""))
                        .build();
                Response response = client.newCall(request).execute();

                if(response.code() == 403){
                    throw new SecurityException("INVALID_CREDENTIALS");
                }

                if (response.isSuccessful()){
                    return MyUser.fromJSON(response.body().string());
                }else{
                    return null;
                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(MyUser myUser) {
            if(myUser == null){
                return;
            }
            else {
                //todo alek
                tvUserName.setText(myUser.getUsername());
                tvPoints.setText(myUser.getPoints());
                tvIncome.setText(myUser.getPoints());
                ivUserColor.setBackgroundColor(myUser.getUserColor());
            }
        }
    }
}
