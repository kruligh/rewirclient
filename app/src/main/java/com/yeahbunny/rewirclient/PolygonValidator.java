package com.yeahbunny.rewirclient;

import android.location.Location;

import java.util.List;

/**
 * Created by Krolis on 2016-12-17.
 */
public class PolygonValidator {

    private static final float MAXIMAL_DISTANCE_FROM_START = 10;
    static private double EPS = 0.000001;

    public static boolean checkIfPolygonIsCreated(List<Location> startLocation, Location newLocation) {

            if(startLocation.size() > 3 && startLocation.get(0).distanceTo(newLocation)<= MAXIMAL_DISTANCE_FROM_START){
                return true;
            }else{
                return false;
            }
    }

}
