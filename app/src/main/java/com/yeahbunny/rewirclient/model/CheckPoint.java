package com.yeahbunny.rewirclient.model;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Krolis on 2016-12-17.
 */
public class CheckPoint {


    public static boolean checkIsValid(Location lastLocation, Location newLocation) {

        long difTimestampSeconds = (newLocation.getTime() - lastLocation.getTime())/1000;

        float distance  = lastLocation.distanceTo(newLocation);

        System.out.println("DISTANCE DIF:" + difTimestampSeconds + " distance: " + distance + " meters v:" + distance/difTimestampSeconds);
    /*        if(distance/difTimestampSeconds >6 || distance/difTimestampSeconds <2 ){
                return false;
            }*/
        return true;
    }
}
