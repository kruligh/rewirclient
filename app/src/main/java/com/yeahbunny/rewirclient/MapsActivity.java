package com.yeahbunny.rewirclient;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.yeahbunny.rewirclient.dto.MyUser;
import com.yeahbunny.rewirclient.dto.NewRewirDto;
import com.yeahbunny.rewirclient.dto.NewRewirResult;
import com.yeahbunny.rewirclient.dto.Pixel;
import com.yeahbunny.rewirclient.model.CheckPoint;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,LocationListener, GoogleMap.OnCameraIdleListener {

    private static final float METERS_TOLERANCE_TO_CLOSE_POLYGON = 10;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private List<Location> mCheckPointsList;
    private List<Polyline> mLines;
    private List<Circle> mCircles;
    private AsyncTask asyncRefreshMap;
    private Button mBtnStartStop;
    private boolean mIsRunning;
    private Polygon polygon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mBtnStartStop = (Button) findViewById(R.id.btnStartStop);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mCheckPointsList = new ArrayList<>();
        mLines = new ArrayList<>();
        mCircles = new LinkedList<>();
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.zoomTo(15));

        mMap.setOnCameraIdleListener(this);




      /*  mMap.addPolygon(new PolygonOptions().addAll(list).strokeColor(Color.GRAY)
                .fillColor(Color.GREEN));*/
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("E","PERMISSION REQUESTED");
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, LocationRequestFactory.createLocationRequest().setNumUpdates(1),this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("onConnectionFailed" + connectionResult);
    }

    @Override
    public void onLocationChanged(Location newLocation) {

        if(!mCheckPointsList.isEmpty()){

            if(PolygonValidator.checkIfPolygonIsCreated(mCheckPointsList, newLocation)){
                onPolygonCompleted();
                return;
            }

            Location lastLocation = mCheckPointsList.get(mCheckPointsList.size()-1);

            mLines.add(mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), new LatLng(newLocation.getLatitude(), newLocation.getLongitude()))
                    .width(3)
                    .color(Color.BLUE)
                    .geodesic(true)));

            if(!CheckPoint.checkIsValid(mCheckPointsList.get(mCheckPointsList.size()-1), newLocation)){
                Toast.makeText(this,"todo wywalic ze za wolno i ze od nowa i wgl wszystko koniec", Toast.LENGTH_SHORT).show();
                //todo wywalic ze za wolno i ze od nowa i wgl wszystko koniec
                mBtnStartStop.performClick();
            }else{
                Toast.makeText(this,"dobreee", Toast.LENGTH_SHORT).show();
            }
        }

        LatLng latLng = new LatLng(newLocation.getLatitude(),newLocation.getLongitude());
     /*   mMap.addMarker(new MarkerOptions().position(latLng).title("ide se"));*/
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        mCheckPointsList.add(newLocation);
    }

    private void showEndDialog(NewRewirResult newRewirResult) {
        //// TODO: 2016-12-18

    }

    private void onPolygonCompleted() {

        Toast.makeText(this,"DONE", Toast.LENGTH_SHORT).show();
        mBtnStartStop.performClick();

        List<LatLng> pointList = new LinkedList<>();

        for(Location location:mCheckPointsList){
            pointList.add(new LatLng(location.getLatitude(),location.getLongitude()));

        }

        for(Polyline line:mLines){
            line.remove();
        }

        polygon = mMap.addPolygon(new PolygonOptions()
                .addAll(pointList)
                .strokeColor(Color.RED)
                .fillColor(Color.BLUE));

        new SendNewRewirAcyncTask().execute(polygon.getPoints());
    }

    public void onButtonStartStopClick(View v) {

        mIsRunning = !mIsRunning;

        if(mIsRunning){

            mCheckPointsList.clear();
            mLines.clear();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e("E","PERMISSION REQUESTED");
                return;
            }

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, LocationRequestFactory.createLocationRequest(),this);

            Toast.makeText(this,"RUN FOREST RUN!!", Toast.LENGTH_SHORT).show();
            ((Button) v).setText(R.string.end_run);

            //Jedna linijka od Alka <3
            mMap.setMyLocationEnabled(true);
        }else{
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
            Toast.makeText(this,"Qniec!!", Toast.LENGTH_SHORT).show();
            ((Button) v).setText(R.string.start_run);
        }
    }

    @Override
    public void onCameraIdle() {
        System.out.println("MOVEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
        //refreshMap();
    }

    private class SendNewRewirAcyncTask extends AsyncTask<List<LatLng>,Void, Integer> {

        private static final String url = Config.SERVER_URL + "/running/import?userid=2";

        @Override
        protected Integer doInBackground(List<LatLng>... params) {
            if(params.length<1){
                return null;
            }

            try {
                OkHttpClient client = new OkHttpClient();

                NewRewirDto newRewirDto = new NewRewirDto(params[0]);

                System.out.println("JSON"+newRewirDto.toJson());

                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), newRewirDto.toJson());

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                        getString(R.string.auth_token), Context.MODE_PRIVATE);

                Request request = new Request.Builder()
                        .url(url)
                        .addHeader(getResources().getString(R.string.auth_token), sharedPref.getString(getResources().getString(R.string.auth_token),""))
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();

                return response.code();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer responseCode) {
            refreshMap(null);
            polygon.remove();

        }
    }

    private class GetMapRewirsAsyncTask extends AsyncTask<Double,Void, List<Pixel>>{


        @Override
        protected List<Pixel> doInBackground(Double... params) {

            if(params.length<4){
                return null;
            }
            try{
                String top = Math.round(params[0] * 10000)+"";
                String bottom = Math.round(params[1] * 10000)+"";
                String right = Math.round(params[3] * 10000)+"";
                String left = Math.round(params[2] * 10000)+"";

              /*  String top = 500608+"";
                String bottom = 500600+"";
                String left = 199330+"";
                String right = 199340+"";*/

                String url = Config.SERVER_URL+"/pixel/getMap?" +
                        "top="+top+"&bottom="+bottom+"&left="+left+"&right="+right;
                System.out.println(url);
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                        getString(R.string.auth_token), Context.MODE_PRIVATE);

                Request request = new Request.Builder()
                        //  .header(getResources().getString(R.string.auth_token), sharedPref.getString(getResources().getString(R.string.auth_token),""))
                        .url(url)
                        .addHeader(getResources().getString(R.string.auth_token), sharedPref.getString(getResources().getString(R.string.auth_token),""))
                        .build();

                OkHttpClient client = new OkHttpClient();

                Response response = client.newCall(request).execute();

                return Pixel.listFromJSON(response.body().string());

            }catch (IOException | JSONException e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Pixel> newPixels) {

            for(Circle circle: mCircles){
                circle.remove();
            }

            for(Pixel pixel :newPixels){
                mCircles.add(mMap.addCircle(new CircleOptions().center(new LatLng(pixel.getLatitude(),pixel.getLongitude())).fillColor(pixel.getColor()).strokeColor(pixel.getColor()).radius(7)));
            }
        }
    }

    public void refreshMap(View view){
        LatLng leftBottom = mMap.getProjection().getVisibleRegion().nearLeft;
        LatLng rightTop = mMap.getProjection().getVisibleRegion().farRight;

        asyncRefreshMap = new GetMapRewirsAsyncTask().execute(rightTop.latitude,leftBottom.latitude,leftBottom.longitude,rightTop.longitude);
    }
}
