package com.yeahbunny.rewirclient;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity {

    EditText tfEmail;
    EditText tfLogin;
    EditText tfPassword;
    EditText tfConfPassword;
    TextView tvHint;
    ImageView ivColor;
    SeekBar sbRed;
    SeekBar sbGreen;
    SeekBar sbBlue;
    Button btSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        tfEmail = (EditText) findViewById(R.id.tfEmail);
        tfLogin = (EditText) findViewById(R.id.tfLogin);
        tfPassword = (EditText) findViewById(R.id.tfPassword);
        tfConfPassword = (EditText) findViewById(R.id.tfConfPassword);
        tvHint = (TextView) findViewById(R.id.tvHint);
        ivColor = (ImageView) findViewById(R.id.ivColor);
        sbRed = (SeekBar) findViewById(R.id.sbRed);
        sbGreen = (SeekBar) findViewById(R.id.sbGreen);
        sbBlue = (SeekBar) findViewById(R.id.sbBlue);
        btSign = (Button) findViewById(R.id.btSign);
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.activity_sign_up);
        mainLayout.requestFocus();

        final RGBColor RGBcolor = new RGBColor(); //to wazne
        //a to mozna posprzatac
        double tRed = ((double)RGBcolor.getRed()/255)*100;
        double tGreen = ((double)RGBcolor.getGreen()/255)*100;
        double tBlue = ((double)RGBcolor.getBlue()/255)*100;
        sbRed.setProgress((int)tRed);
        sbGreen.setProgress((int)tGreen);
        sbBlue.setProgress((int)tBlue);

        ivColor.setBackgroundColor(Color.rgb(RGBcolor.getRed(), RGBcolor.getGreen(), RGBcolor.getBlue()));

        sbRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float barValue = Float.parseFloat(String.valueOf(progress));
                int colValue = Math.round((barValue/100)*255);
                ivColor.setBackgroundColor(Color.rgb(colValue, RGBcolor.getGreen(), RGBcolor.getBlue()));
                RGBcolor.setRed(colValue);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        sbGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float barValue = Float.parseFloat(String.valueOf(progress));
                int colValue = Math.round((barValue/100)*255);
                ivColor.setBackgroundColor(Color.rgb(RGBcolor.getRed(),colValue, RGBcolor.getBlue()));
                RGBcolor.setGreen(colValue);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        sbBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float barValue = Float.parseFloat(String.valueOf(progress));
                int colValue = Math.round((barValue/100)*255);
                ivColor.setBackgroundColor(Color.rgb(RGBcolor.getRed(), RGBcolor.getGreen(),colValue));
                RGBcolor.setBlue(colValue);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    public void onClickSignComplete(View view) {

        if (validate(tfEmail.getText().toString(),tfLogin.getText().toString(),tfPassword.getText().toString(),tfConfPassword.getText().toString()) == true) {
            //TODO wypierdalam z tąd
        }
    }

    public boolean validate(String email, String login, String password, String cpassword) {
        //Email validation
        if (email.isEmpty()) {
            tvHint.setText("E-mail is empty");
            return false;
        }
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches() == false) {
            tvHint.setText("Incorrect e-mail address");
            return false;
        }

        //Login validation
        if (login.isEmpty()) {
            tvHint.setText("Login is empty");
            return false;
        }
        if (login.length() < 5) {
            tvHint.setText("Login is too short");
            return false;
        }
        if (login.matches("^(?:\\W|\\s)*$")) {
            tvHint.setText("Use only alphanumeric");
            return false;
        }
        //Password validation
        if (password.isEmpty()) {
            tvHint.setText("Password is empty");
            return false;
        }
        if (password.length() < 8) {
            tvHint.setText("Password is too short");
            return false;
        }
        //ConfPassword validation
        if (cpassword.isEmpty()) {
            tvHint.setText("Confirm your password");
            return false;
        }
        if (password.equals(cpassword) == false) {
            tvHint.setText("Passwords aren't the same");
            return false;
        }
        tvHint.setText("");
        return true;
    }
}
