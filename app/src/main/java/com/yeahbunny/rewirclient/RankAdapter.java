package com.yeahbunny.rewirclient;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RankAdapter extends ArrayAdapter<RankUser> {
    private final Context context;
    private final ArrayList<RankUser> itemsArrayList;

    public RankAdapter(Context context, ArrayList<RankUser> itemsArrayList) {
        super(context, R.layout.activity_rank, itemsArrayList);
        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rank_row, parent, false);
        TextView labelView = (TextView) rowView.findViewById(R.id.label);
        TextView valueView = (TextView) rowView.findViewById(R.id.value);
        labelView.setText(itemsArrayList.get(position).getUserName());
        valueView.setText(itemsArrayList.get(position).getUserPoints());
        return rowView;
    }
}