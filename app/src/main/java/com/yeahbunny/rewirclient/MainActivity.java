package com.yeahbunny.rewirclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.Polygon;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.yeahbunny.rewirclient.dto.LoginRequest;
import com.yeahbunny.rewirclient.dto.NewRewirDto;
import com.yeahbunny.rewirclient.dto.NewRewirResult;

import org.json.JSONException;

import java.io.IOException;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView tvLogo2;
    TextView tvHint;
    EditText tfLogin;
    EditText tfPassword;
    Button btLogin;
    Button btSign;
    ImageView bcgLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvLogo2 = (TextView) findViewById(R.id.tvLogo2);
        tvHint = (TextView) findViewById(R.id.tvHint);
        tfLogin = (EditText) findViewById(R.id.tfLogin);
        tfPassword = (EditText) findViewById(R.id.tfPassword);
        btLogin = (Button) findViewById(R.id.btLogin);
        btSign = (Button) findViewById(R.id.btSign);
        bcgLogin = (ImageView) findViewById(R.id.bcgLogin);
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.activity_main);
        mainLayout.requestFocus();


        tfLogin.setText("jamesBond");
        tfPassword.setText("asdasd");
    }

    public void changeTip(View view) {
        //moj easter egg <3
        String[] tips = {"Yolo", "Make tests green again!", "Beka KSH", "KrolikXMyk", "Unit tests? Never heard of it", ""};
        int index = new Random().nextInt(tips.length);
        tvLogo2.setText(tips[index]);
    }

    public void onClickLogin(View view) {

        if (validate(tfLogin.getText().toString(), tfPassword.getText().toString()) == true) {

            try{
                new LoginAsyncTask().execute(tfLogin.getText().toString(), tfPassword.getText().toString());
            }catch (SecurityException e){
                tvHint.setText("Invalid login or password");
            }
        }
    }

    public void onClickGoToMap(View view) {
        Intent intent = new Intent(this,MapsActivity.class);
        startActivityForResult(intent,0);
    }

    public void onClickSign(View view) {
        Intent intent = new Intent(this,SignUpActivity.class);
        startActivityForResult(intent,0);
    }

    public boolean validate(String login, String password) {
        //Login validation
        if (login.isEmpty()) {
            tvHint.setText("Login is empty");
            return false;
        }
        if (login.length() < 5) {
            tvHint.setText("Login is too short");
            return false;
        }
        if (login.matches("^(?:\\W|\\s)*$")) {
            tvHint.setText("Use only alphanumeric");
            return false;
        }
        //Password validation
        if (password.isEmpty()) {
            tvHint.setText("Password is empty");
            return false;
        }
        if (password.length() < 4) {
            tvHint.setText("Password is too short");
            return false;
        }
        tvHint.setText("");
        return true;
    }

    private class LoginAsyncTask extends AsyncTask<String,Void, String> {

        private static final String url = Config.SERVER_URL+"/core/login";

        @Override
        protected String doInBackground(String... params) {
            try {
                OkHttpClient client = new OkHttpClient();
                LoginRequest loginRequest = new LoginRequest(params[0], params[1]);
                RequestBody body;
                try{
                    body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), loginRequest.toJson());
                }catch (JSONException e){
                    e.printStackTrace();
                    return null;
                }

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();

                if(response.code() == 403){
                    return null;
                }

                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
           if(result == null || result.isEmpty()){
               tvHint.setText("Invalid login or password");
               return;
           }else{
               SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);

               sharedPreferences.edit().putString(getResources().getString(R.string.auth_token),result).apply();

               System.out.println(sharedPreferences.getString(getResources().getString(R.string.auth_token),null));

               Intent intent = new Intent(getApplicationContext(),UserPanelActivity.class);
               startActivityForResult(intent,0);

           }
        }
    }
}

