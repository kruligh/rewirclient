package com.yeahbunny.rewirclient;

public class RankUser {
    private String userName;
    private String userPoints;

    public RankUser(String title, String description) {
        super();
        this.userName = title;
        this.userPoints = description;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserPoints() {
        return userPoints;
    }
    public void setUserPoints(String userPoints) {
        this.userPoints = userPoints;
    }
}