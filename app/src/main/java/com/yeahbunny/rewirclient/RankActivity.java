package com.yeahbunny.rewirclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class RankActivity extends AppCompatActivity {

    ListView listView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);
        listView = (ListView) findViewById(R.id.listRank);

        RankAdapter adapter = new RankAdapter(this, generateData());
        listView.setAdapter(adapter);
    }
    private ArrayList<RankUser> generateData(){
        ArrayList<RankUser> items = new ArrayList<RankUser>();
        items.add(new RankUser("★★★★★ Bait Hackaton 2016","NaN points"));
        items.add(new RankUser("★★★★ Mr. Robot","10110101 points"));
        items.add(new RankUser("★★★ Geralt of Tibia","52 and a half points"));
        items.add(new RankUser("★★ Pegasus","9999999 in 1 points"));
        items.add(new RankUser("★ xbox one","792 points"));
        items.add(new RankUser("Dominik","420 points"));
        items.add(new RankUser("Piotrek","192.168.1.100 points"));
        items.add(new RankUser("Alek","One 8-bit pixel point"));
        return items;
    }
}